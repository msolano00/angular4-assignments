import { Component, OnInit } from '@angular/core';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  totalCounter = this.counterService.generalCounter;
  activeCounter = this.counterService.activeCounter;
  inactiveCounter = this.counterService.inactiveCounter;

  constructor(private counterService: CounterService) {
    this.counterService.counterUpdate.subscribe((data: {total: number, active: number, inactive: number}) => {
      this.totalCounter = data.total;
      this.activeCounter = data.active;
      this.inactiveCounter = data.inactive;
    });
  }

  ngOnInit() {
  }

}
