import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class CounterService {

  generalCounter: number = 0;
  activeCounter: number = 0;
  inactiveCounter: number = 0;

  constructor() { }

  counterUpdate: EventEmitter<{total: number, active: number, inactive: number}> = new EventEmitter<{total: number, active: number, inactive: number}>();

  generalAdd() {
    this.generalCounter++;
    console.log(`General: ${this.generalCounter}`);
  }

  activeAdd() {
    this.generalAdd();
    this.activeCounter++;
    this.counterUpdate.emit({'total': this.generalCounter, 'active': this.activeCounter, 'inactive': this.inactiveCounter});
    console.log(`Active: ${this.activeCounter}`);
  }

  inactiveAdd() {
    this.generalAdd();
    this.inactiveCounter++;
    this.counterUpdate.emit({'total': this.generalCounter, 'active': this.activeCounter, 'inactive': this.inactiveCounter});
    console.log(`Inactive: ${this.inactiveCounter}`);
  }
}
